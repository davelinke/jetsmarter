# README #

This is an AngularJS exercise to propose a basic prototype of flight operation panel.

you can check a running example of this implementation at http://davelinke.com/jetsmarter/

Requirements:

* NodeJS
* NPM

Setup:

* Clone Repo
* Open Node Prompt
* cd repo directory
* npm install
* grunt
